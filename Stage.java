import java.util.Scanner;

public class Stage {
    public String caption;
    public String description;
    public Action[] actions;
    private int selectedActionIndex = -1;

    public Stage(String caption, String description, Action[] actions) {
        this.caption = caption;
        this.description = description;
        this.actions = actions;
    }

    public void show(){
        System.out.println(caption);
        System.out.println(description);
        System.out.println();
        for(int i = 0; i < actions.length; i++){
            System.out.print(i+1 + ".");
            actions[i].show();
        }
    }
    public void reset(){
        selectedActionIndex = -1;
    }

    public Action getSelectedAction(){
        while (selectedActionIndex < 0 || selectedActionIndex >= actions.length){
            Scanner in = new Scanner(System.in);
            System.out.print("make your choise:");
            selectedActionIndex = in.nextInt() - 1;
        }
        System.out.println();
        System.out.println();
        return actions[selectedActionIndex];

    }
}
