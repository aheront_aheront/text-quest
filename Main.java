import java.util.Arrays;
import java.util.Scanner;
public class Main {
    static Quest[] levels = new Quest[]{
            new Quest("The Nightmare",new Resources(5,5,5), new Stage[]
                    {

                            new Stage("New Day",
                                    "you woke up and wanna sleep again",
                                    new Action[]{
                                            new Action("go to bed", new Resources(-2, 3,2), "Sometimes you need to work."),
                                            new Action("go work", new Resources(4, -3, -1), "You're was very unhappy. But don't worry, be happy next time"),
                                            new Action("drink coffee", new Resources(-1, -1, 2), "\"MORE COFFEE\" - you said and died")
                                    }),
                            new Stage("Evening",
                                    "you returned home and you're bored but you should finish your work tasks",
                                    new Action[]{
                                            new Action("go to bed", new Resources(-3, 4, 2), "Sometimes you need to work."),
                                            new Action("go work", new Resources(2, -2, -3), "Sometimes you need to sleep. Monitor your health next time"),
                                            new Action("watch netflix", new Resources(-1, -1, 6), "Die from laughing. How? We don't know")
                                    }),
                            new Stage("Night", "you was woken up at 3 o'clock by a nightmare",
                                    new Action[]{
                                            new Action("go to psychologist", new Resources(-10, 2, 1), "He has just killed you. Don't go to psychologist at 3 o'clock, he wnna sleep too"),
                                            new Action("try to suicide (suicide is very bad, think about your friends)", new Resources(-1, -100, -100), "Why did you do that? Ok, you just wanna check the gameover"),
                                            new Action("go sleep", new Resources(0,1,-2), "Monsters in your nightmare was too scary.")
                                    })
                    })
    };

    public static void showLevels(){
        System.out.println();
        for(int i = 1; i <= levels.length; i++){
            System.out.println(i + "." + levels[i-1].name);
        }
        System.out.println();
        System.out.println("0. Exit the game");
    }
    public static void main(String[] args) {
        int choice = -1;

        while (choice != 0) {
            showLevels();
            while (choice < 0 || (choice - 1) >= levels.length) {
                Scanner in = new Scanner(System.in);
                System.out.println();
                System.out.print("Choose level:");
                choice = in.nextInt();
            }
            if (choice == 0) break;

            levels[choice - 1].startQuest();
            choice = -1;
        }


    }

}
