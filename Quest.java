import java.util.Scanner;

public class Quest {
    public  String name;
    private Resources startResources;
    private Resources resources;
    private Stage[] stages;
    private int currentStageIndex = 0;
    private Boolean repeatQuestAgreement = true;
    private Action selectedAction;

    public Quest(String name, Resources startResources, Stage[] stages) {
        this.name = name;
        this.startResources = startResources;
        this.stages = stages;
    }

    private boolean isAlive() {
        return resources.money > 0 &&
                resources.health > 0 &&
                resources.happiness > -5 && resources.happiness < 10;
    }

    public void reset(){
        resources = new Resources(startResources);
        currentStageIndex = 0;
        Action selectedAction = new Action("filler", new Resources(0,0,0), "if you see it, start resources was set up wrong");
        for(int i = 0 ; i < stages.length; i++) {
            stages[i].reset();
        }
    }


    public void startQuest() {
        System.out.println();
        repeatQuestAgreement = true;
        while (repeatQuestAgreement) {
            reset();

            while (isAlive()) {
                if (currentStageIndex >= stages.length) {
                    System.out.println("You win!! \nEnd of Quest");
                    return;
                }
                System.out.printf("money:%d health:%d happiness:%d\n", resources.money, resources.health, resources.happiness);
                stages[currentStageIndex].show();
                selectedAction = stages[currentStageIndex].getSelectedAction();
                resources.applyChange(selectedAction.resourceChange);

                currentStageIndex++;
            }
            System.out.println(selectedAction.dieCause);
            repeatQuestAgreement = null;
            while (repeatQuestAgreement == null) {
                System.out.println("You die. \nWant to repeat?(y/n)");
                Scanner in = new Scanner(System.in);
                char choise = (char) in.next().charAt(0);
                if (choise == 'y') repeatQuestAgreement = true;
                if (choise == 'n') repeatQuestAgreement = false;
            }
            System.out.println();
            System.out.println();
        }
    }
}
