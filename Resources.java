public class Resources {
    public int money;
    public int health;
    public int happiness;

    public Resources(int money, int health, int happiness) {
        this.money = money;
        this.health = health;
        this.happiness = happiness;
    }

    public Resources(Resources resources){
        this.money = resources.money;
        this.health = resources.health;
        this.happiness = resources.happiness;
    }
    public void applyChange(Resources change){
        money += change.money;
        health += change.health;
        happiness += change.happiness;
    }


}
