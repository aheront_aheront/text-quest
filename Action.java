public class Action {
    public String name;
    public Resources resourceChange;
    public String dieCause;

    public Action(String name, Resources resourceChange, String dieCause) {
        this.resourceChange = resourceChange;
        this.name = name;
        this.dieCause = dieCause;
    }
    public void show(){
        System.out.println(name);
    }
}
